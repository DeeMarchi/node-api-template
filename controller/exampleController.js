module.exports = {

    get: async (req, res, next) => {
        try {
            res.json({ message: 'Hello world' });
        } catch (err) {
            /* If any error occurs, send it to the error handling middlewares */
            next(err)
        }
    }
}
