const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const exampleRouter = require('./routes/example');
const errors = require('./errorHandling/middlewares');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', exampleRouter);

/* Error handling middlewares */
app.use(errors.notFound);
app.use(errors.printStackTrace);
app.use(errors.catchAll);

/* App port should be in env, otherwise defaults to 3000 */
app.listen(process.env.PORT ?? 3000);

module.exports = app;
