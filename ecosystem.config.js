module.exports = {
	apps: [{
			script: 'app.js',
			name: 'TEMPLATE_APP',
			watch: true,
			ignore_watch: [ 'node_modules' ],
			node_args: '-r dotenv/config',
			env_production: {
				NODE_ENV: "production"
			},
			env_development: {
				NODE_ENV: "development_work",
			},
			env_homologacao: {
				NODE_ENV: "homologacao"
			}
		},
	],
};
