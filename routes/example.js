const express = require('express');
const router = express.Router();

const exampleController = require('../controller/exampleController');

router.get('/', exampleController.get);

module.exports = router;
